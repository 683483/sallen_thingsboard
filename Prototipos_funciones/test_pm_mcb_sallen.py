# -*- coding: utf-8 -*-
"""
Created on Tue Jun 21 16:01:19 2022

@author: l.lax
"""
import numpy as np
from scipy.stats import kurtosis, skew

SIZE_HEADER = 7;
SIZE_DATOS_MAQUINA = 1036;

SIZE_Ind_bill_I_t = 16;
SIZE_Ind_bill_16_t = 14;
SIZE_Ind_bill_int16_t = 14;
SIZE_INDICADORES_BILLETE = 2 * SIZE_Ind_bill_I_t + 2 * SIZE_Ind_bill_16_t + 8;

SIZE_Ind_fajo_16_t = 98;
SIZE_Ind_fajo_I_t = 112;
SIZE_INDICADORES_FAJO = 2 * SIZE_Ind_fajo_I_t + 2 * SIZE_Ind_fajo_16_t + 4 * SIZE_Ind_bill_16_t;


def read_subindicador(content, first_byte, tipo_indicador):
    
    subindicadores = {};
    if tipo_indicador == 'Ind_bill_I_t':    
        subindicadores['eficaz'] = int.from_bytes(content[first_byte+0:first_byte+2], byteorder='little');
        subindicadores['media'] = int.from_bytes(content[first_byte+2:first_byte+4], byteorder='little');
        subindicadores['mediana'] = int.from_bytes(content[first_byte+4:first_byte+6], byteorder='little');
        subindicadores['max'] = int.from_bytes(content[first_byte+6:first_byte+8], byteorder='little');
        subindicadores['min'] = int.from_bytes(content[first_byte+8:first_byte+10], byteorder='little', signed=True);
        subindicadores['std'] = int.from_bytes(content[first_byte+10:first_byte+12], byteorder='little');
        subindicadores['skew'] = int.from_bytes(content[first_byte+12:first_byte+14], byteorder='little', signed=True);
        subindicadores['curtosis'] = int.from_bytes(content[first_byte+14:first_byte+16], byteorder='little', signed=True);
    elif tipo_indicador == 'Ind_bill_16_t':    
        subindicadores['media'] = int.from_bytes(content[first_byte+0:first_byte+2], byteorder='little');
        subindicadores['mediana'] = int.from_bytes(content[first_byte+2:first_byte+4], byteorder='little');
        subindicadores['max'] = int.from_bytes(content[first_byte+4:first_byte+6], byteorder='little');
        subindicadores['min'] = int.from_bytes(content[first_byte+6:first_byte+8], byteorder='little');
        subindicadores['std'] = int.from_bytes(content[first_byte+8:first_byte+10], byteorder='little');
        subindicadores['skew'] = int.from_bytes(content[first_byte+10:first_byte+12], byteorder='little', signed=True);
        subindicadores['curtosis'] = int.from_bytes(content[first_byte+12:first_byte+14], byteorder='little', signed=True); 
    elif tipo_indicador == 'Ind_bill_int16_t':
        subindicadores['media'] = int.from_bytes(content[first_byte+0:first_byte+2], byteorder='little', signed=True);
        subindicadores['mediana'] = int.from_bytes(content[first_byte+2:first_byte+4], byteorder='little', signed=True);
        subindicadores['max'] = int.from_bytes(content[first_byte+4:first_byte+6], byteorder='little', signed=True);
        subindicadores['min'] = int.from_bytes(content[first_byte+6:first_byte+8], byteorder='little', signed=True);
        subindicadores['std'] = int.from_bytes(content[first_byte+8:first_byte+10], byteorder='little', signed=True);
        subindicadores['skew'] = int.from_bytes(content[first_byte+10:first_byte+12], byteorder='little', signed=True);
        subindicadores['curtosis'] = int.from_bytes(content[first_byte+12:first_byte+14], byteorder='little', signed=True);
    
    return subindicadores;


def read_subindicador_fajo(content, first_byte, tipo_indicador):
    
    subindicadores_fajo = {};
    if tipo_indicador == 'Ind_fajo_16_t': 
        subindicadores_fajo['media'] = read_subindicador(content, first_byte, 'Ind_bill_16_t')
        subindicadores_fajo['mediana'] = read_subindicador(content, first_byte + SIZE_Ind_bill_16_t, 'Ind_bill_16_t');
        subindicadores_fajo['max'] = read_subindicador(content, first_byte + 2 * SIZE_Ind_bill_16_t, 'Ind_bill_16_t');
        subindicadores_fajo['min'] = read_subindicador(content, first_byte + 3 * SIZE_Ind_bill_16_t, 'Ind_bill_16_t');
        subindicadores_fajo['std'] = read_subindicador(content, first_byte + 4 * SIZE_Ind_bill_16_t, 'Ind_bill_16_t');
        subindicadores_fajo['skew'] = read_subindicador(content, first_byte + 5 * SIZE_Ind_bill_16_t, 'Ind_bill_int16_t');
        subindicadores_fajo['curtosis'] = read_subindicador(content, first_byte + 5 * SIZE_Ind_bill_16_t + SIZE_Ind_bill_int16_t, 'Ind_bill_int16_t');
    elif tipo_indicador == 'Ind_fajo_I_t':    
        subindicadores_fajo['eficaz'] = read_subindicador(content, first_byte, 'Ind_bill_16_t')
        subindicadores_fajo['media'] = read_subindicador(content, first_byte + SIZE_Ind_bill_16_t, 'Ind_bill_16_t');
        subindicadores_fajo['mediana'] = read_subindicador(content, first_byte + 2 * SIZE_Ind_bill_16_t, 'Ind_bill_16_t');
        subindicadores_fajo['max'] = read_subindicador(content, first_byte + 3 * SIZE_Ind_bill_16_t, 'Ind_bill_16_t');
        subindicadores_fajo['min'] = read_subindicador(content, first_byte + 4 * SIZE_Ind_bill_16_t, 'Ind_bill_16_t');
        subindicadores_fajo['std'] = read_subindicador(content, first_byte + 5 * SIZE_Ind_bill_16_t, 'Ind_bill_16_t');
        subindicadores_fajo['skew'] = read_subindicador(content, first_byte + 6 * SIZE_Ind_bill_16_t, 'Ind_bill_int16_t');
        subindicadores_fajo['curtosis'] = read_subindicador(content, first_byte + 6 * SIZE_Ind_bill_16_t + SIZE_Ind_bill_int16_t, 'Ind_bill_int16_t');

    return subindicadores_fajo;


def read_indicadores_fajo(content, first_byte):
    indicadores_fajo = {};

    indicadores_fajo['I_trans'] = read_subindicador_fajo(content, first_byte, 'Ind_fajo_I_t');
    indicadores_fajo['I_alim'] = read_subindicador_fajo(content, first_byte + SIZE_Ind_fajo_I_t, 'Ind_fajo_I_t');
    indicadores_fajo['n_pulsos_trans'] = read_subindicador_fajo(content, first_byte + 2 * SIZE_Ind_fajo_I_t, 'Ind_fajo_16_t');
    indicadores_fajo['n_pulsos_alim'] = read_subindicador_fajo(content, first_byte + 2 * SIZE_Ind_fajo_I_t + SIZE_Ind_fajo_16_t, 'Ind_fajo_16_t');
    indicadores_fajo['T_IR1a1b'] = read_subindicador(content, first_byte + 2 * SIZE_Ind_fajo_I_t + 2 * SIZE_Ind_fajo_16_t, 'Ind_bill_16_t');
    indicadores_fajo['T_IR1b3a'] = read_subindicador(content, first_byte + 2 * SIZE_Ind_fajo_I_t + 2 * SIZE_Ind_fajo_16_t + SIZE_Ind_bill_16_t, \
                                                          'Ind_bill_16_t');
    indicadores_fajo['T_IR3a3b'] = read_subindicador(content, first_byte + 2 * SIZE_Ind_fajo_I_t + 2 * SIZE_Ind_fajo_16_t + 2 * SIZE_Ind_bill_16_t, \
                                                          'Ind_bill_16_t');
    indicadores_fajo['T_IR1b2'] = read_subindicador(content, first_byte + 2 * SIZE_Ind_fajo_I_t + 2 * SIZE_Ind_fajo_16_t + 3 * SIZE_Ind_bill_16_t, \
                                                         'Ind_bill_16_t');
    
    return indicadores_fajo;


def read_indicadores_billete(content, first_byte, number_banknotes):
    indicadores_billete = [];
    
    for n_banknote in range(0, number_banknotes):
        indicadores_billete_temp = {};
        first_banknote_byte = first_byte + n_banknote * SIZE_INDICADORES_BILLETE;
        indicadores_billete_temp['I_trans'] = read_subindicador(content, first_banknote_byte, 'Ind_bill_I_t');
        indicadores_billete_temp['I_alim'] = read_subindicador(content, first_banknote_byte + 1 * SIZE_Ind_bill_I_t, 'Ind_bill_I_t');
        indicadores_billete_temp['n_pulsos_trans'] = read_subindicador(content, first_banknote_byte + 2 * SIZE_Ind_bill_I_t, 'Ind_bill_16_t');
        indicadores_billete_temp['n_pulsos_alim'] = read_subindicador(content, first_banknote_byte + 2 * SIZE_Ind_bill_I_t + SIZE_Ind_bill_16_t, 'Ind_bill_16_t');
        first_byte_temp = first_banknote_byte + 2 * SIZE_Ind_bill_I_t + 2 * SIZE_Ind_bill_16_t;
        indicadores_billete_temp['T_IR1a1b'] = int.from_bytes(content[first_byte_temp:first_byte_temp+2], byteorder='little');
        indicadores_billete_temp['T_IR1b3a'] = int.from_bytes(content[first_byte_temp+2:first_byte_temp+4], byteorder='little');
        indicadores_billete_temp['T_IR3a3b'] = int.from_bytes(content[first_byte_temp+4:first_byte_temp+6], byteorder='little');
        indicadores_billete_temp['T_IR1b2'] = int.from_bytes(content[first_byte_temp+6:first_byte_temp+8], byteorder='little');
        
        indicadores_billete.append(indicadores_billete_temp);
    
    return indicadores_billete;


if __name__ == "__main__":    

    with open('Data_deposit/20_banknotes/acceptor_sr120_1630915278622_4564266_58_mcb.dat', 'rb') as fh:
        content = fh.read();
        
    #Number banknotes
    number_banknotes = content[1];
    
    #Read indicadores fajo
    indicadores_mot_fajo = read_indicadores_fajo(content, SIZE_HEADER)
    
    #Read indicadores billete
    indicadores_mot_billete =read_indicadores_billete(content, SIZE_HEADER + SIZE_INDICADORES_FAJO, number_banknotes)

    indicadores_mot_fajo_2 = {}
    nombres_ind_billete = ['media', 'mediana', 'max', 'min', 'std', 'skew', 'curtosis']
    nombres_medidas_mot = ['I_trans', 'I_alim', 'n_pulsos_trans', 'n_pulsos_alim', 'T_IR1a1b', 'T_IR1b3a', 'T_IR3a3b',
                           'T_IR1b2']
    for measure in nombres_medidas_mot:
        indicadores_mot_fajo_2[measure] = {}
        if measure[0] == 'T':
            nombres_indicadores = ['valor']
        elif measure[0] == 'I':
            nombres_indicadores = ['eficaz'] + nombres_ind_billete
        else:
            nombres_indicadores = nombres_ind_billete
        for indicador_fajo in nombres_indicadores:
            indicadores_mot_fajo_2[measure][indicador_fajo] = {}
            for name_indicador in nombres_ind_billete:
                indicadores_mot_fajo_2[measure][indicador_fajo][name_indicador] = 0

    # Va pasando por cada una de las medidas tomadas
    for name_meas_f in nombres_medidas_mot:
        if name_meas_f[0] == 'I':
            nombres_indicadores_f = ['eficaz'] + nombres_ind_billete
        elif name_meas_f[0] == 'T':
            nombres_indicadores_f = ['valor']
        else:
            nombres_indicadores_f = nombres_ind_billete
        for name_indicador_f in nombres_indicadores_f:
            valores_ind_b_l = []
            for num_bill in range(len(indicadores_mot_billete)):
                if name_meas_f[0] == 'T':
                    valores_ind_b_l.append(indicadores_mot_billete[num_bill][name_meas_f])
                else:
                    valores_ind_b_l.append(indicadores_mot_billete[num_bill][name_meas_f][name_indicador_f])
            valores_ind_b = np.array(valores_ind_b_l)
            valores_ind_f = [valores_ind_b.mean(),
                             np.median(valores_ind_b),
                             valores_ind_b.max(),
                             valores_ind_b.min(),
                             valores_ind_b.std(),
                             skew(valores_ind_b),
                             kurtosis(valores_ind_b)]
            for name_indicador_b, value2 in zip(nombres_ind_billete, valores_ind_f):
                indicadores_mot_fajo_2[name_meas_f][name_indicador_f][name_indicador_b] = value2
    pass