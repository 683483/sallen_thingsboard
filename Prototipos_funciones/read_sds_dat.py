# -*- coding: utf-8 -*-
"""
Created on Mon Jan  3 16:23:20 2022

@author: l.lax
"""
import numpy as np
from controller.sallen_tb_device import*
import argparse
import sys
from FUNCIONES_MOTORES import*
import time
import os
import glob

SIZE_HEADER = 7;
#SIZE_DATOS_MAQUINA = 1036;
SIZE_INDICADORES_BILLETE = 116;
SIZE_INDICADORES_FAJO = 788;

DOBLES_SAMPLES = 80;
SIZE_DOBLES_SAMPLES = 4;
V_SAMPLES = 98;
SIZE_V_SAMPLES = 4;
TEMP_SAMPLES = 1;
SIZE_TEMP_SAMPLES = 4;
EXTRA_BYTE_BANKNOTE = 0;

AUDIO_SAMPLES = 10;
SIZE_AUDIO_SAMPLES = 4;

SIZE_DATOS_MAQUINA = (2 * DOBLES_SAMPLES * SIZE_DOBLES_SAMPLES \
                      + 2 * V_SAMPLES * SIZE_V_SAMPLES + TEMP_SAMPLES * SIZE_TEMP_SAMPLES + EXTRA_BYTE_BANKNOTE);


def read_datos_maquina(content, number_banknotes):
    first_byte = SIZE_HEADER + SIZE_INDICADORES_FAJO + number_banknotes * SIZE_INDICADORES_BILLETE;
    
    datos_maquina = {};
    datos_maquina['dobles1'] = np.empty([number_banknotes, DOBLES_SAMPLES]);
    datos_maquina['dobles2'] = np.empty([number_banknotes, DOBLES_SAMPLES]);
    datos_maquina['v_int'] = np.empty([number_banknotes, V_SAMPLES]);
    datos_maquina['v_aux'] = np.empty([number_banknotes, V_SAMPLES]);
    datos_maquina['temp'] = np.empty([number_banknotes, 1]);
    
    for n_banknote in range(0, number_banknotes):
        first_byte_banknote = first_byte + n_banknote * (2 * DOBLES_SAMPLES * SIZE_DOBLES_SAMPLES \
                                 + 2 * V_SAMPLES * SIZE_V_SAMPLES + TEMP_SAMPLES * SIZE_TEMP_SAMPLES + EXTRA_BYTE_BANKNOTE);
        first_byte_doubles_1 = first_byte_banknote + 0 * DOBLES_SAMPLES * SIZE_DOBLES_SAMPLES;
        first_byte_doubles_2 = first_byte_banknote + 1 * DOBLES_SAMPLES * SIZE_DOBLES_SAMPLES;
        first_byte_v_int = first_byte_banknote + 2 * DOBLES_SAMPLES * SIZE_DOBLES_SAMPLES + 0 * V_SAMPLES * SIZE_V_SAMPLES;
        first_byte_v_aux = first_byte_banknote + 2 * DOBLES_SAMPLES * SIZE_DOBLES_SAMPLES + 1 * V_SAMPLES * SIZE_V_SAMPLES;
        first_byte_temperature = first_byte_banknote + 2 * DOBLES_SAMPLES * SIZE_DOBLES_SAMPLES + 2 * V_SAMPLES * SIZE_V_SAMPLES;
        
        for n_double_sample in range(0, DOBLES_SAMPLES * SIZE_DOBLES_SAMPLES, SIZE_DOBLES_SAMPLES):
            sample = int(n_double_sample / SIZE_DOBLES_SAMPLES);
            datos_maquina['dobles1'][n_banknote,sample] = content[first_byte_doubles_1 + sample * SIZE_DOBLES_SAMPLES + 0] * 2**0;
            datos_maquina['dobles1'][n_banknote,sample] += content[first_byte_doubles_1 + sample * SIZE_DOBLES_SAMPLES + 1] * 2**8;
            datos_maquina['dobles1'][n_banknote,sample] += content[first_byte_doubles_1 + sample * SIZE_DOBLES_SAMPLES + 2] * 2**16;
            datos_maquina['dobles1'][n_banknote,sample] += content[first_byte_doubles_1 + sample * SIZE_DOBLES_SAMPLES + 3] * 2**24;
            datos_maquina['dobles2'][n_banknote,sample] = content[first_byte_doubles_2 + sample * SIZE_DOBLES_SAMPLES + 0] * 2**0;
            datos_maquina['dobles2'][n_banknote,sample] += content[first_byte_doubles_2 + sample * SIZE_DOBLES_SAMPLES + 1] * 2**8;
            datos_maquina['dobles2'][n_banknote,sample] += content[first_byte_doubles_2 + sample * SIZE_DOBLES_SAMPLES + 2] * 2**16;
            datos_maquina['dobles2'][n_banknote,sample] += content[first_byte_doubles_2 + sample * SIZE_DOBLES_SAMPLES + 3] * 2**24;
            
        for n_v_sample in range(0, V_SAMPLES * SIZE_V_SAMPLES, SIZE_V_SAMPLES):
            sample = int(n_v_sample / SIZE_V_SAMPLES);
            datos_maquina['v_int'][n_banknote,sample] = content[first_byte_v_int + sample * SIZE_V_SAMPLES + 0] * 2**0;
            datos_maquina['v_int'][n_banknote,sample] += content[first_byte_v_int + sample * SIZE_V_SAMPLES + 1] * 2**8;
            datos_maquina['v_aux'][n_banknote,sample] = content[first_byte_v_aux + sample * SIZE_V_SAMPLES + 0] * 2**0;
            datos_maquina['v_aux'][n_banknote,sample] += content[first_byte_v_aux + sample * SIZE_V_SAMPLES + 1] * 2**8;

        datos_maquina['temp'][n_banknote,0] = content[first_byte_temperature];
        datos_maquina['temp'][n_banknote,0] += content[first_byte_temperature + 1] * 2**8;
    
    return datos_maquina;


def read_indicadores_billete(content, number_banknotes):
    indicadores = {};
    indicadores['dobles1'] = {};
    indicadores['dobles1']['media'] = np.empty([number_banknotes]);
    indicadores['dobles1']['mediana'] = np.empty([number_banknotes]);
    indicadores['dobles1']['max'] = np.empty([number_banknotes]);
    indicadores['dobles1']['min'] = np.empty([number_banknotes]);
    indicadores['dobles1']['std'] = np.empty([number_banknotes]);
    indicadores['dobles1']['skew'] = np.empty([number_banknotes]);
    indicadores['dobles1']['curtosis'] = np.empty([number_banknotes]);
    indicadores['dobles2'] = {};
    indicadores['dobles2']['media'] = np.empty([number_banknotes]);
    indicadores['dobles2']['mediana'] = np.empty([number_banknotes]);
    indicadores['dobles2']['max'] = np.empty([number_banknotes]);
    indicadores['dobles2']['min'] = np.empty([number_banknotes]);
    indicadores['dobles2']['std'] = np.empty([number_banknotes]);
    indicadores['dobles2']['skew'] = np.empty([number_banknotes]);
    indicadores['dobles2']['curtosis'] = np.empty([number_banknotes]);
    indicadores['v_int'] = {};
    indicadores['v_int']['media'] = np.empty([number_banknotes]);
    indicadores['v_int']['mediana'] = np.empty([number_banknotes]);
    indicadores['v_int']['max'] = np.empty([number_banknotes]);
    indicadores['v_int']['min'] = np.empty([number_banknotes]);
    indicadores['v_int']['std'] = np.empty([number_banknotes]);
    indicadores['v_int']['skew'] = np.empty([number_banknotes]);
    indicadores['v_int']['curtosis'] = np.empty([number_banknotes]);
    indicadores['v_aux'] = {};
    indicadores['v_aux']['media'] = np.empty([number_banknotes]);
    indicadores['v_aux']['mediana'] = np.empty([number_banknotes]);
    indicadores['v_aux']['max'] = np.empty([number_banknotes]);
    indicadores['v_aux']['min'] = np.empty([number_banknotes]);
    indicadores['v_aux']['std'] = np.empty([number_banknotes]);
    indicadores['v_aux']['skew'] = np.empty([number_banknotes]);
    indicadores['v_aux']['curtosis'] = np.empty([number_banknotes]);
    indicadores['temp'] = {};
    indicadores['temp']['media'] = np.empty([number_banknotes]);
    
    first_byte = SIZE_HEADER + SIZE_INDICADORES_FAJO;
    
    for n_banknote in range(0, number_banknotes):
        first_byte_banknote = first_byte + n_banknote * SIZE_INDICADORES_BILLETE;
        
        
        indicadores['dobles1']['media'][n_banknote] = int.from_bytes(content[first_byte_banknote+0:first_byte_banknote+4], byteorder='little');
        indicadores['dobles1']['mediana'][n_banknote] = int.from_bytes(content[first_byte_banknote+4:first_byte_banknote+8], byteorder='little');
        indicadores['dobles1']['max'][n_banknote] =  int.from_bytes(content[first_byte_banknote+8:first_byte_banknote+12], byteorder='little');
        indicadores['dobles1']['min'][n_banknote] = int.from_bytes(content[first_byte_banknote+12:first_byte_banknote+16], byteorder='little');
        indicadores['dobles1']['std'][n_banknote] = int.from_bytes(content[first_byte_banknote+16:first_byte_banknote+20], byteorder='little');
        indicadores['dobles1']['skew'][n_banknote] = int.from_bytes(content[first_byte_banknote+20:first_byte_banknote+24], byteorder='little', signed=True);
        indicadores['dobles1']['curtosis'][n_banknote] = int.from_bytes(content[first_byte_banknote+24:first_byte_banknote+28], byteorder='little', signed=True);

        indicadores['dobles2']['media'][n_banknote] = int.from_bytes(content[first_byte_banknote+28:first_byte_banknote+32], byteorder='little');
        indicadores['dobles2']['mediana'][n_banknote] = int.from_bytes(content[first_byte_banknote+32:first_byte_banknote+36], byteorder='little');
        indicadores['dobles2']['max'][n_banknote] = int.from_bytes(content[first_byte_banknote+36:first_byte_banknote+40], byteorder='little');
        indicadores['dobles2']['min'][n_banknote] = int.from_bytes(content[first_byte_banknote+40:first_byte_banknote+44], byteorder='little');
        indicadores['dobles2']['std'][n_banknote] = int.from_bytes(content[first_byte_banknote+44:first_byte_banknote+48], byteorder='little');
        indicadores['dobles2']['skew'][n_banknote] = int.from_bytes(content[first_byte_banknote+48:first_byte_banknote+52], byteorder='little', signed=True);
        indicadores['dobles2']['curtosis'][n_banknote] = int.from_bytes(content[first_byte_banknote+52:first_byte_banknote+56], byteorder='little', signed=True);

        indicadores['v_int']['media'][n_banknote] = int.from_bytes(content[first_byte_banknote+56:first_byte_banknote+60], byteorder='little');
        indicadores['v_int']['mediana'][n_banknote] = int.from_bytes(content[first_byte_banknote+60:first_byte_banknote+64], byteorder='little');
        indicadores['v_int']['max'][n_banknote] = int.from_bytes(content[first_byte_banknote+64:first_byte_banknote+68], byteorder='little');
        indicadores['v_int']['min'][n_banknote] = int.from_bytes(content[first_byte_banknote+68:first_byte_banknote+72], byteorder='little');
        indicadores['v_int']['std'][n_banknote] = int.from_bytes(content[first_byte_banknote+72:first_byte_banknote+76], byteorder='little');
        indicadores['v_int']['skew'][n_banknote] = int.from_bytes(content[first_byte_banknote+76:first_byte_banknote+80], byteorder='little', signed=True);
        indicadores['v_int']['curtosis'][n_banknote] = int.from_bytes(content[first_byte_banknote+80:first_byte_banknote+84], byteorder='little', signed=True);

        indicadores['v_aux']['media'][n_banknote] = int.from_bytes(content[first_byte_banknote+84:first_byte_banknote+88], byteorder='little');
        indicadores['v_aux']['mediana'][n_banknote] = int.from_bytes(content[first_byte_banknote+88:first_byte_banknote+92], byteorder='little');
        indicadores['v_aux']['max'][n_banknote] = int.from_bytes(content[first_byte_banknote+92:first_byte_banknote+96], byteorder='little');
        indicadores['v_aux']['min'][n_banknote] = int.from_bytes(content[first_byte_banknote+96:first_byte_banknote+100], byteorder='little');
        indicadores['v_aux']['std'][n_banknote] = int.from_bytes(content[first_byte_banknote+100:first_byte_banknote+104], byteorder='little');
        indicadores['v_aux']['skew'][n_banknote] = int.from_bytes(content[first_byte_banknote+104:first_byte_banknote+108], byteorder='little', signed=True);
        indicadores['v_aux']['curtosis'][n_banknote] = int.from_bytes(content[first_byte_banknote+108:first_byte_banknote+112], byteorder='little', signed=True);
        
        indicadores['temp']['media'][n_banknote] = int.from_bytes(content[first_byte_banknote+112:first_byte_banknote+116], byteorder='little');
    
    return indicadores;


def read_subindicadores_fajo(content, n_variable, n_indicador, signo):
    first_byte = SIZE_HEADER + n_variable * 7 * 7 * 4 + n_indicador * 7 * 4;
    
    subindicadores_fajo = {};
    if signo == 1:
        subindicadores_fajo['media'] = int.from_bytes(content[first_byte+0:first_byte+4], byteorder='little', signed=True);
        subindicadores_fajo['mediana'] = int.from_bytes(content[first_byte+4:first_byte+8], byteorder='little', signed=True);
        subindicadores_fajo['max'] = int.from_bytes(content[first_byte+8:first_byte+12], byteorder='little', signed=True);
        subindicadores_fajo['min'] = int.from_bytes(content[first_byte+12:first_byte+16], byteorder='little', signed=True);
        subindicadores_fajo['std'] = int.from_bytes(content[first_byte+16:first_byte+20], byteorder='little', signed=True);
        subindicadores_fajo['skew'] = int.from_bytes(content[first_byte+20:first_byte+24], byteorder='little', signed=True);
        subindicadores_fajo['curtosis'] = int.from_bytes(content[first_byte+24:first_byte+28], byteorder='little', signed=True);
    else:
        subindicadores_fajo['media'] = int.from_bytes(content[first_byte+0:first_byte+4], byteorder='little');
        subindicadores_fajo['mediana'] = int.from_bytes(content[first_byte+4:first_byte+8], byteorder='little');
        subindicadores_fajo['max'] = int.from_bytes(content[first_byte+8:first_byte+12], byteorder='little');
        subindicadores_fajo['min'] = int.from_bytes(content[first_byte+12:first_byte+16], byteorder='little');
        subindicadores_fajo['std'] = int.from_bytes(content[first_byte+16:first_byte+20], byteorder='little');
        subindicadores_fajo['skew'] = int.from_bytes(content[first_byte+20:first_byte+24], byteorder='little', signed=True);
        subindicadores_fajo['curtosis'] = int.from_bytes(content[first_byte+24:first_byte+28], byteorder='little', signed=True);
    
    return subindicadores_fajo;


def read_indicadores_fajo(content):
    indicadores_fajo = {};
    indicadores_fajo['dobles1'] = {};
    indicadores_fajo['dobles1']['media'] = read_subindicadores_fajo(content, 0, 0, 0);
    indicadores_fajo['dobles1']['mediana'] = read_subindicadores_fajo(content, 0, 1, 0);
    indicadores_fajo['dobles1']['max'] = read_subindicadores_fajo(content, 0, 2, 0);
    indicadores_fajo['dobles1']['min'] = read_subindicadores_fajo(content, 0, 3, 0);
    indicadores_fajo['dobles1']['std'] = read_subindicadores_fajo(content, 0, 4, 0);
    indicadores_fajo['dobles1']['skew'] = read_subindicadores_fajo(content, 0, 5, 1);
    indicadores_fajo['dobles1']['curtosis'] = read_subindicadores_fajo(content, 0, 6, 1);
    
    indicadores_fajo['dobles2'] = {};
    indicadores_fajo['dobles2']['media'] = read_subindicadores_fajo(content, 1, 0, 0);
    indicadores_fajo['dobles2']['mediana'] = read_subindicadores_fajo(content, 1, 1, 0);
    indicadores_fajo['dobles2']['max'] = read_subindicadores_fajo(content, 1, 2, 0);
    indicadores_fajo['dobles2']['min'] = read_subindicadores_fajo(content, 1, 3, 0);
    indicadores_fajo['dobles2']['std'] = read_subindicadores_fajo(content, 1, 4, 0);
    indicadores_fajo['dobles2']['skew'] = read_subindicadores_fajo(content, 1, 5, 1);
    indicadores_fajo['dobles2']['curtosis'] = read_subindicadores_fajo(content, 1, 6, 1);
    
    indicadores_fajo['v_int'] = {};
    indicadores_fajo['v_int']['media'] = read_subindicadores_fajo(content, 2, 0, 0);
    indicadores_fajo['v_int']['mediana'] = read_subindicadores_fajo(content, 2, 1, 0);
    indicadores_fajo['v_int']['max'] = read_subindicadores_fajo(content, 2, 2, 0);
    indicadores_fajo['v_int']['min'] = read_subindicadores_fajo(content, 2, 3, 0);
    indicadores_fajo['v_int']['std'] = read_subindicadores_fajo(content, 2, 4, 0);
    indicadores_fajo['v_int']['skew'] = read_subindicadores_fajo(content, 2, 5, 1);
    indicadores_fajo['v_int']['curtosis'] = read_subindicadores_fajo(content, 2, 6, 1);
    
    indicadores_fajo['v_aux'] = {};
    indicadores_fajo['v_aux']['media'] = read_subindicadores_fajo(content, 3, 0, 0);
    indicadores_fajo['v_aux']['mediana'] = read_subindicadores_fajo(content, 3, 1, 0);
    indicadores_fajo['v_aux']['max'] = read_subindicadores_fajo(content, 3, 2, 0);
    indicadores_fajo['v_aux']['min'] = read_subindicadores_fajo(content, 3, 3, 0);
    indicadores_fajo['v_aux']['std'] = read_subindicadores_fajo(content, 3, 4, 0);
    indicadores_fajo['v_aux']['skew'] = read_subindicadores_fajo(content, 3, 5, 1);
    indicadores_fajo['v_aux']['curtosis'] = read_subindicadores_fajo(content, 3, 6, 1);
    
    first_byte = SIZE_HEADER + 4 * 7 * 7 * 4;
    indicadores_fajo['temp'] = int.from_bytes(content[first_byte+0:first_byte+4], byteorder='little');
    
    return indicadores_fajo;


def read_audio_data(content, number_banknotes):
    first_audio_data_byte = SIZE_HEADER + SIZE_INDICADORES_FAJO + number_banknotes * (SIZE_INDICADORES_BILLETE + SIZE_DATOS_MAQUINA);
    audio_data = np.empty([number_banknotes, AUDIO_SAMPLES]);
    
    for n_banknote in range(0, number_banknotes):
        for n_audio_sample in range(0, AUDIO_SAMPLES * SIZE_AUDIO_SAMPLES, SIZE_AUDIO_SAMPLES):
            sample = int(n_audio_sample / SIZE_AUDIO_SAMPLES);
            first_byte = first_audio_data_byte + n_banknote * AUDIO_SAMPLES * SIZE_AUDIO_SAMPLES + \
                sample * SIZE_AUDIO_SAMPLES;
            audio_data[n_banknote,sample] = int.from_bytes(content[first_byte:first_byte+4], byteorder='little', signed=True);
            
    return audio_data;
     

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--ruta', action='store', type=str,
                        default='Data_deposit/50_banknotes/',
                        help='Ruta del fichero a analizar')
    arg = parser.parse_args(sys.argv[1:])

    handler = SallenTBDevice(host='tb.lab-01.howlab.es', port=1883, name='Maquina_Sallen',
                             access_token='p8xGAhCPXodaLKX6mCVr', dir_name=arg.ruta)

    handler.connect()
    while True:
        handler.check_files_to_send()
        time.sleep(5)

    # with open(arg.ruta, 'rb') as fh:
    #     content = fh.read()
    #
    # #Number banknotes
    # number_banknotes = content[1] + content[2] * 256;
    #
    # #Read datos maquina
    # datos_maquina = read_datos_maquina(content, number_banknotes);
    #
    # #Read indicadores billete
    # indicadores_billete = read_indicadores_billete(content, number_banknotes);
    #
    # #Read indicadores fajo
    # indicadores_fajo = read_indicadores_fajo(content);
    #
    # #Read audio data
    # audio_data = read_audio_data(content, number_banknotes);

    pass