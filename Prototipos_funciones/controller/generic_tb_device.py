from tb_device_mqtt import TBDeviceMqttClient, TBPublishInfo
import logging
import sys
from concurrent.futures import ThreadPoolExecutor

from .tb_utils import *

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler(sys.stdout))


class GenericTBDevice:
    name: str = None
    label: str = None

    __access_token = None
    __provisioning_device_key = None
    __provisioning_secret_key = None

    __client: TBDeviceMqttClient = None

    __registered = False
    __host = 'localhost'
    __port = 1883

    __process_request = None

    __shared_attributes: dict = None
    __client_attributes: dict = None

    @property
    def is_connected(self):
        return self.__client is not None and self.__client.is_connected()

    def __init__(self, host, port, name, label=None, access_token=None, provision_key=None, provision_secret=None,
                 on_rpc_request=None, client_attributes_keys=[], shared_attributes_keys=[]):
        self.__host = host
        self.__port = port

        self.name = name
        self.label = label or name

        self.__access_token = access_token
        self.__provisioning_device_key = provision_key
        self.__provisioning_secret_key = provision_secret

        self.__shared_attributes: dict = {key: None for key in client_attributes_keys}
        self.__client_attributes: dict = {key: None for key in shared_attributes_keys}

        if callable(on_rpc_request):
            self.__process_request = on_rpc_request

    def register(self, host=None, port=None, access_token=None, provision_key=None, provision_secret=None):
        if self.__registered:
            return

        logger.info('Registering...')
        if not host:
            host = self.__host
        if not port:
            port = self.__port
        if not provision_key:
            provision_key = self.__provisioning_device_key
        if not provision_secret:
            provision_secret = self.__provisioning_secret_key
        if not access_token:
            access_token = self.__access_token

        credentials = TBDeviceMqttClient.provision(host=host, port=port,
                                                   provision_device_key=provision_key,
                                                   provision_device_secret=provision_secret,
                                                   device_name=self.name, access_token=access_token)
        if credentials == access_token:
            self.__host = host
            self.__port = port
            self.__access_token = access_token
            self.__provisioning_device_key = provision_key
            self.__provisioning_secret_key = provision_secret
            self.__registered = True
            logger.info('Done...')
            return True
        else:
            logger.warning('Error while registering!')
            return False

    def connect(self, host=None, port=None, access_token=None):
        self.disconnect()

        if not host:
            host = self.__host
        if not port:
            port = self.__port
        if not access_token:
            access_token = self.__access_token

        client = TBDeviceMqttClient(host=host, port=port, token=access_token)

        logger.info('Connecting...')
        client.connect()
        if not client.is_connected():
            logger.warning('Error while connecting!')
            return False

        self.__client = client

        client.set_server_side_rpc_request_handler(self.__on_rpc_request)
        client.subscribe_to_all_attributes(self.__on_attributes_updated)
        client.request_attributes(client_keys=list(self.__client_attributes.keys()), callback=self.__on_attributes_updated)

        logger.info('Done...')

        return self.is_connected

    def disconnect(self):
        if self.is_connected:
            logger.info('Disconnecting...')
            self.__client.set_server_side_rpc_request_handler(None)
            self.__client.disconnect()
            logger.info('Done.')

        self.__client = None

    def __on_rpc_request(self, client, request_id, request_body):


        logger.debug(f'Received RPC {request_body}')

        method = request_body['method']
        params = request_body['params']

        if self.__process_request:
            ex = ThreadPoolExecutor()
            ex.submit(self.__process_request, method, params)

    def __on_attributes_updated(self, client, result, exception):

        if exception is not None:
            logger.warning('Error retrieving attributes', exception)
            return

        if not 'client' in result:
            return

        cs_attributes = result['client']
        if 'total_takings' in cs_attributes:
            self.__total_takings = cs_attributes['total_takings']
        if 'coin_pocket' in cs_attributes:
            self.__coin_pocket = cs_attributes['coin_pocket']
        if 'credit' in cs_attributes:
            self.__credit = cs_attributes['credit']
        if 'balls_out' in cs_attributes:
            self.__balls_out = cs_attributes['balls_out']
        if 'total_balls' in cs_attributes:
            self.__total_balls = cs_attributes['total_balls']
        if 'balls_in' in cs_attributes:
            self.__total_balls = self.__balls_out + cs_attributes['balls_in']

    def update_attributes(self, attributes):

        if not self.is_connected:
            return False

        logger.debug(f'Sending attributes update {attributes}...')
        result = self.__client.send_attributes(attributes)
        publish_info = result.get()
        if publish_info == TBPublishInfo.TB_ERR_SUCCESS:
            logger.debug('Done sending attributes')
            return True
        else:
            logger.warning(f'Error while sending attributes [publish_info={publish_info}]', )
            return False

    def update_telemetry(self, values=None, timestamp=None, telemetry=None):

        if not self.is_connected:
            return False

        if telemetry is None:
            telemetry = get_tb_telemetry_json(values=values, timestamp=timestamp)

        logger.debug(f'Sending telemetry {telemetry}...')
        result = self.__client.send_telemetry(telemetry=telemetry)
        publish_info = result.get()
        if publish_info == TBPublishInfo.TB_ERR_SUCCESS:
            logger.debug('Done sending telemetry')
            return True
        else:
            logger.warning(f'Error while sending telemetry [publish_info={publish_info}]', )
            return False

