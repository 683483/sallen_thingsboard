#include "extraccion_indicadores.h"

#define SUBSAMPLING_RATIO 		1		//Define para reducir el número de samples según la memoria disponible, en caso de subsamplear se calcularía una media cada SUBSAMPLING_RATIO medidas
#define I_TRANS_SAMPLES			1500	//Valores de I_trans capturados por billete desde IR1a hasta IR3b/IR2
#define I_ALIM_SAMPLES			1250	//Valores de I_alim capturados por billete desde 300ms antes del paso por GBR
#define N_PULSOS_TRANS_SAMPLES 	1000	//Valores de n_pulsos_trans capturados por billete desde IR1a hasta IR3b/IR2
#define N_PULSOS_ALIM_SAMPLES 	125     //Valores de n_pulsos_alim capturados por billete desde 300ms antes del paso por GBR

#define DOBLES_SAMPLES 			80		//Valores de los sensores de dobles capturados por billete durante 80ms desde el paso por GBR
#define V_SAMPLES 				100		//Valores de V_int y V_aux capturados por billete durante 100ms desde el paso por GBR

#define FFT_SIZE 				512		//Valores obtenidos del cálculo de una FFT de 1024 puntos

struct Datos_motores_t {	//Con el subsampling de 1: 7758 Bytes
	uint16_t  I_trans[I_TRANS_SAMPLES / SUBSAMPLING_RATIO];					
	uint16_t  I_alim[I_ALIM_SAMPLES / SUBSAMPLING_RATIO];
	uint16_t  n_pulsos_trans[N_PULSOS_TRANS_SAMPLES / SUBSAMPLING_RATIO];
	uint16_t  n_pulsos_alim[N_PULSOS_ALIM_SAMPLES / SUBSAMPLING_RATIO];
	uint16_t  T_IR1a1b;
	uint16_t  T_IR1b3a;
	uint16_t  T_IR3a3b;
	uint16_t  T_IR1b2;
} datos_motores;

struct Datos_CIS_t {	//Con el subsampling de 1: 1042 Bytes
	uint32_t  dobles1[DOBLES_SAMPLES / SUBSAMPLING_RATIO];
	uint32_t  dobles2[DOBLES_SAMPLES / SUBSAMPLING_RATIO];
	uint16_t  V_int[V_SAMPLES / SUBSAMPLING_RATIO];
	uint16_t  V_aux[V_SAMPLES / SUBSAMPLING_RATIO];
	uint16_t  temperatura;
} datos_CIS;

struct Datos_STWIN_t {	//Con el subsampling de 1: 1 Bytes
	uint16_t  FFT[FFT_SIZE];
} datos_STWIN;

struct Datos_maquina_t {	//Con el subsampling de 1: 9824 Bytes
	Datos_motores_t datos_motores;
	Datos_CIS_t datos_CIS;
	Datos_STWIN_t datos_STWIN;
} datos_maquina;


/**************************CAPA INDICADORES POR BILLETE******************************/
struct Ind_bill_I_t {		//Tamaño: 16 Bytes
	uint16_t Eficaz;		//Se añade el indicador asociado al valor eficaz
	uint16_t Media;
	uint16_t Mediana;
	uint16_t Maximo;
	uint16_t Minimo;
	uint16_t Desv_est;
	uint16_t Skew;
	uint16_t Curtosis;
} 

struct Ind_bill_t {			//Tamaño: 14 Bytes
	uint16_t Media;
	uint16_t Mediana;
	uint16_t Maximo;
	uint16_t Minimo;
	uint16_t Desv_est;
	uint16_t Skew;
	uint16_t Curtosis;
} 

struct Indicadores_billete_motores_t {		//Tamaño: 68 Bytes
	Ind_bill_I_t  indi_bill_I_trans;
	Ind_bill_I_t  indi_bill_I_alim;
	Ind_bill_t  indi_bill_n_pulsos_trans;
	Ind_bill_t  indi_bill_n_pulsos_alim;
	uint16_t  T_IR1a1b;
	uint16_t  T_IR1b3a;
	uint16_t  T_IR3a3b;
	uint16_t  T_IR1b2;
} indicadores_billete_motores;
	
struct Indicadores_billete_CIS_t {			//Tamaño: 58 Bytes
	Ind_bill_t  indi_bill_dobles1;
	Ind_bill_t  indi_bill_dobles2;
	Ind_bill_t  indi_bill_V_int;
	Ind_bill_t  indi_bill_V_aux;
	uint16_t  temperatura;				//Se calcula la media de la temperatura durante el paso del billete
} indicadores_billete_CIS;
	
#define	N_SLOTS_SAMPLING	10
struct Slot_FFT_t {			//Tamaño: 4 Bytes
	uint8_t F_ini;
	uint8_t F_fin;
	uint16_t Energia;
}
/*Se divide el rango de las frecuencias de la FFT en N_SLOTS_SAMPLING intervalos de los que se indica en centenas de Hz las frecuencias de los límites (F_ini y F_fin)*/
struct Indicadores_billete_STWIN_t {		//Tamaño con N_SLOTS_SAMPLING = 10: 40 Bytes
	Slot_FFT_t slots[N_SLOTS_SAMPLING];
} indicadores_billete_STWIN;

struct Indicadores_capa_billete_t {			//Tamaño con N_SLOTS_SAMPLING = 10: 166 Bytes
	Indicadores_billete_motores_t indicadores_billete_motores;
	Indicadores_billete_CIS_t indicadores_billete_CIS;
	Indicadores_billete_STWIN_t indicadores_billete_STWIN;
} indicadores_billete;
/*
Entran los datos que la máquina toma de la trama de motores -> datos_motores
Se extraen los indicadores por billete de la trama de motores -> indicadores_billete_motores
*/
uint8_t calcular_indicadores_billete_motores(Datos_motores_t *datos_motores, Indicadores_billete_motores_t *indicadores_billete_motores)

/*
Entran los datos que la máquina toma de la trama de CIS -> datos_CIS
Se extraen los indicadores por billete de la trama de CIS -> indicadores_billete_CIS
*/
uint8_t calcular_indicadores_billete_CIS(Datos_CIS_t *datos_CIS, Indicadores_billete_CIS_t *indicadores_billete_CIS)

/*
Entran los datos que la máquina toma de la trama de STWIN -> datos_STWIN
Se extraen los indicadores por billete de la trama de STWIN -> indicadores_billete_STWIN
*/
uint8_t calcular_indicadores_billete_STWIN(Datos_STWIN_t_t *datos_STWIN, Indicadores_billete_STWIN_t *indicadores_billete_STWIN)

/*
Entran los datos que la máquina toma durante el paso del billete enventanados según las tramas -> datos_maquina
Se extraen los indicadores correspondientes -> indicadores_billete
*/ 
uint8_t calcular_indicadores_billete(Datos_maquina_t *datos_maquina, Indicadores_capa_billete_t *indicadores_billete)

/**************************CAPA INDICADORES POR FAJO******************************/
struct Ind_fajo_t {					//Tamaño: 128 Bytes
	Ind_bill_t ind_fajo_Media;
	Ind_bill_t ind_fajo_Mediana;
	Ind_bill_t ind_fajo_Maximo;
	Ind_bill_t ind_fajo_Minimo;
	Ind_bill_t ind_fajo_Desv_est;
	Ind_bill_t ind_fajo_Skew;
	Ind_bill_t ind_fajo_Curtosis;
} 

struct Ind_fajo_I_t {				//Tamaño: 98 Bytes
	Ind_bill_t ind_fajo_Eficaz;			//Se añaden los indicadores asociados al valor eficaz
	Ind_bill_t ind_fajo_Media;
	Ind_bill_t ind_fajo_Mediana;
	Ind_bill_t ind_fajo_Maximo;
	Ind_bill_t ind_fajo_Minimo;
	Ind_bill_t ind_fajo_Desv_est;
	Ind_bill_t ind_fajo_Skew;
	Ind_bill_t ind_fajo_Curtosis;
}

struct Indicadores_fajo_motores_t {	//Tamaño: 508 Bytes
	Ind_fajo_I_t  indi_fajo_I_trans;
	Ind_fajo_I_t  indi_fajo_I_alim;
	Ind_fajo_t  indi_fajo_n_pulsos_trans;
	Ind_fajo_t  indi_fajo_n_pulsos_alim;
	Ind_bill_t  indi_fajo_T_IR1a1b;		//Al tener de indicador por billete un único valor por billete, los indicadores por fajo son del tipo equivalente al Ind_bill_t
	Ind_bill_t  indi_fajo_T_IR1b3a;		//Al tener de indicador por billete un único valor por billete, los indicadores por fajo son del tipo equivalente al Ind_bill_t
	Ind_bill_t  indi_fajo_T_IR3a3b;		//Al tener de indicador por billete un único valor por billete, los indicadores por fajo son del tipo equivalente al Ind_bill_t
	Ind_bill_t  indi_fajo_T_IR1b2;		//Al tener de indicador por billete un único valor por billete, los indicadores por fajo son del tipo equivalente al Ind_bill_t
   } indicadores_fajo_motores;
	
struct Indicadores_fajo_CIS_t {		//Tamaño: 394 Bytes
   Ind_fajo_t  indi_fajo_dobles1;
   Ind_fajo_t  indi_fajo_dobles2;
   Ind_fajo_t  indi_fajo_V_int;
   Ind_fajo_t  indi_fajo_V_aux;
   uint16_t  reg_lin_temperatura;		//Se calcula la regresión lineal de la evolución de la temperatura a lo largo del paso de todo el fajo
} indicadores_fajo_CIS;

struct fajo_Slots_FFT_t {			//Tamaño: 16 Bytes
	uint8_t F_ini;
	uint8_t F_fin;
	Ind_bill_t indi_fajo_Energia;		//Al tener de indicador por billete un único valor por billete, los indicadores por fajo son del tipo equivalente al Ind_bill_t
}
struct Indicadores_fajo_STWIN_t {	//Tamaño con N_SLOTS_SAMPLING = 10: 160 Bytes
	fajo_Slots_FFT_t  fajo_slots[N_SLOTS_SAMPLING];
} indicadores_fajo_STWIN;

struct Indicadores_fajo_t {			//Tamaño con N_SLOTS_SAMPLING = 10: 1062 Bytes
	Indicadores_fajo_motores_t indicadores_fajo_motores;
	Indicadores_fajo_CIS_t indicadores_fajo_CIS;
	Indicadores_fajo_STWIN_t indicadores_fajo_STWIN;
} indicadores_fajo;

/*
Entran los indicadores por billete de [TAMANO_FAJO] billetes -> indicadores_billete
Se extraen los indicadores por fajo correspondientes -> indicadores_fajo
*/
#define TAMANO_FAJO
uint8_t calcular_indicadores_fajo(Indicadores_capa_billete_t *indicadores_billete[TAMANO_FAJO], Indicadores_fajo_t *indicadores_fajo)

