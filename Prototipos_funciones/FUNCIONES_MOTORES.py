import numpy as np

# MOTORES

SIZE_HEADER_T = 7

# DATOS EN CRUDO
# from read_sds_dat import SIZE_INDICADORES_FAJO

I_TRANS_SAMPLES = 1500
I_ALIM_SAMPLES  = 1250
N_PULSOS_TRANS_SAMPLES  =   1000
N_PULSOS_ALIM_SAMPLES   =   125
motores_num_samples_list = [I_TRANS_SAMPLES, I_ALIM_SAMPLES, N_PULSOS_TRANS_SAMPLES, N_PULSOS_ALIM_SAMPLES, 1, 1, 1, 1]
motores_origins_measures = [0]
for element in motores_num_samples_list[:-1]:
    motores_origins_measures.append(element+motores_origins_measures[-1])
motores_measures_names = ['I_trans', 'I_alim', 'n_pulsos_trans', 'n_pulsos_alim', 'T_IR1a1b', 'T_IR1b3a', 'T_IR3a3b',
                          'T_IR1b2']
SIZE_DATA_MOTORES = 2
SIZE_DATA_BILLETE_MOTORES   =  [num_samples*SIZE_DATA_MOTORES for num_samples in
                                motores_num_samples_list]


def read_datos_maquina_motores(data, number_banknotes):
    datos_maquina = {}
    datos_maquina['I_trans']    = np.empty([number_banknotes, I_TRANS_SAMPLES])
    datos_maquina['I_alim']     = np.empty([number_banknotes, I_ALIM_SAMPLES])
    datos_maquina['n_pulsos_trans'] = np.empty([number_banknotes, N_PULSOS_TRANS_SAMPLES])
    datos_maquina['n_pulsos_alim'] = np.empty([number_banknotes, N_PULSOS_ALIM_SAMPLES])
    datos_maquina['T_IR1a1b']   = np.empty([number_banknotes, 1])
    datos_maquina['T_IR1b3a']   = np.empty([number_banknotes, 1])
    datos_maquina['T_IR3a3b']   = np.empty([number_banknotes, 1])
    datos_maquina['T_IR1b2']    = np.empty([number_banknotes, 1])

    # Va pasando por todos los billetes detectados
    for n_banknote in range(0, number_banknotes):
        # Va pasando por cada una de las magnitudes medidas para cada billete
        for name, num_samples, origin in zip(motores_measures_names, motores_num_samples_list,
                                             motores_origins_measures):
            # Va pasando por cada una de las muestras tomadas para cada medida
            for n_data in range(origin, num_samples):
                value = int.from_bytes(
                    data[(origin+n_data*SIZE_DATA_MOTORES+n_banknote*motores_origins_measures[-1]):(origin+(
                            n_data+1)*SIZE_DATA_MOTORES)],
                    'big',
                    signed=False)
                datos_maquina[name][n_banknote, n_data] = value

    return  datos_maquina


# INDICADORES DE BILLETE
SIZE_IND_T = 2
SIZE_IND_I = 16
SIZE_IND_16b = 14
size_indicadores_billete_por_medida = [SIZE_IND_I, SIZE_IND_I, SIZE_IND_16b, SIZE_IND_16b, SIZE_IND_T, SIZE_IND_T,
                                       SIZE_IND_T, SIZE_IND_T]
motores_origins_indicadores = [476] #Hay 476 bytes de indicadores de fajo
for element in size_indicadores_billete_por_medida[:-1]:
    motores_origins_indicadores.append(element+motores_origins_indicadores[-1])
indicadores_billete_names = ['media', 'mediana', 'max', 'min', 'std', 'skew', 'curtosis']
indicadores_billete_names_I = ['eficaz', 'media', 'mediana', 'max', 'min', 'std', 'skew', 'curtosis']

#INDICADORES POR FAJO
SIZE_IND_FAJO = 2

def read_indicadores_motores(data, number_banknotes):
    indicadores_mot_billete = {}
    for measure in motores_measures_names:
        indicadores_mot_billete[measure] = {}
        if measure[0] == 'T':
            indicadores_mot_billete[measure]['valor'] = np.empty([number_banknotes])
        else:
            if measure[0] == 'I':
                indicadores_mot_billete[measure]['eficaz'] = np.empty([number_banknotes])
            for name_indicador in indicadores_billete_names:
                indicadores_mot_billete[measure][name_indicador] = np.empty([number_banknotes])

    indicadores_mot_fajo = {}
    for measure in motores_measures_names:
        indicadores_mot_fajo[measure] = {}
        if measure[0] == 'T':
            nombres_indicadores = ['valor']
        elif measure[0] == 'I':
            nombres_indicadores = ['eficaz']+indicadores_billete_names
        else:
            nombres_indicadores = indicadores_billete_names
        for indicador_fajo in nombres_indicadores:
            indicadores_mot_fajo[measure][indicador_fajo] = {}
            for name_indicador in indicadores_billete_names:
                indicadores_mot_fajo[measure][indicador_fajo][name_indicador] = 0

    start = -2 #Hago que empiece en -2 para no poner un contador para multiplicar x2 para sacar el start
    # Va pasando por cada una de las medidas tomadas
    for name_meas_f in motores_measures_names:
        if name_meas_f[0] == 'I':
            nombres_indicadores_f = ['eficaz'] + indicadores_billete_names
        elif name_meas_f[0] == 'T':
            nombres_indicadores_f = ['valor']
        else:
            nombres_indicadores_f = indicadores_billete_names
        for name_indicador_f in nombres_indicadores_f:
            if name_indicador_f == 'skew' or name_indicador_f == 'curtosis':
                signos_b = [True, True, True, True, True, True, True]
            else:
                signos_b = [False, False, False, False, False, True, True]
            for name_indicador_b, signo_b in zip(indicadores_billete_names,signos_b):
                start = start + 2
                end = start + 2
                value2 = int.from_bytes(data[start:end], byteorder='little', signed=signo_b)
                indicadores_mot_fajo[name_meas_f][name_indicador_f][name_indicador_b] = value2

    # Va pasando por todos los billetes detectados
    for n_banknote in range(0, number_banknotes):
        # Va pasando por cada una de las medidas tomadas
        for name_meas, origin in zip(motores_measures_names, motores_origins_indicadores):
            # Para tener en cuenta el indicador valor eficaz de las medidas de corriente
            if name_meas[0] == 'I':
                nombres_indicadores = ['eficaz'] + indicadores_billete_names
                signos = [False, False, False, False, True, False, True, True]
            elif name_meas[0] == 'T':
                nombres_indicadores = ['valor']
                signos = [False]
            else:
                nombres_indicadores = indicadores_billete_names
                signos = [False, False, False, False, False, True, True]
            # Va pasando por los indicadores de cada una de las medidas
            for n_indicador, (name_indicador, signo) in enumerate(zip(nombres_indicadores,signos)):
                #7 bytes de cabecera + byte de comienzo de los indicadores de una medida concreta + bytes para el comienzo
                # de un indicador concreto de la medida + bytes para el comienzo del billete buscado
                start = SIZE_HEADER_T + origin + SIZE_IND_T * n_indicador + n_banknote * (motores_origins_indicadores[-1] + 2)
                end = start + SIZE_IND_T
                value1 = int.from_bytes(data[start:end], byteorder='little', signed=signo)
                indicadores_mot_billete[name_meas][name_indicador][n_banknote] = value1


    return  indicadores_mot_billete, indicadores_mot_fajo
